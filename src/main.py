import os
import os.path as op
import pickle
import random

from taipy.gui import  Icon, notify
import taipy as tp

import requests


HOME_DIR = os.getcwd()
SRC_DIR = op.join(HOME_DIR,'src')
DATA_DIR =  op.join(SRC_DIR, 'data')
MODELS_DIR = op.join(SRC_DIR,'models')
IMAGES_DIR = op.join(SRC_DIR, 'images')

with open(op.join(DATA_DIR, 'true_labels.pkl'), 'rb') as f:
    true_labels = pickle.load(f)
    if not op.exists(DATA_DIR):
        os.makedirs(DATA_DIR)


def select_images(directory, extension='.jpg'):
    selected = []

    # Loop through the files in the directory
    for file_name in os.listdir(directory):
        file_path = os.path.join(directory, file_name)
        if os.path.isfile(file_path) and file_name.endswith(extension):
            selected.append(file_path)

    return selected


def predict_image(file_name):

    # res = requests.get(f"http://localhost:8001/classify/{file_name}")  # , #fapi
    res = requests.get(f"https://iloncka-mosal-api.hf.space/classify?image_name={file_name}")
    # json=payload)
    res = res.json()
    # print(res)

    if requests.codes.ok:
        prediction =  res["prediction"]
        score = res["score"]
        return  prediction, score
    else:
        return "error", "error"


img_path = op.join(IMAGES_DIR, 'placeholder.jpg')
logo = op.join(IMAGES_DIR, 'logo-mosal.png')

test_images = select_images(DATA_DIR)


def build_dn_partial(list_img):
    selected_image = None
    render_selector = True
    partial_content = "<|part|render={render_selector}|\n\n"

    selected_image = None
    partial_content += "<|{selected_image}|selector|lov={list_img}|on_change={on_change}|width=100vw|>"
    # partial_content += "|>\n\n"
    return partial_content

rand_imgs = []
for _ in range(5):
    rand_imgs.append(test_images[random.randint(0, len(test_images)-1)])

list_img = [(rand_imgs[0], Icon(op.join('src/data', op.basename(rand_imgs[0])), f"{op.basename(rand_imgs[0])}")),
        (rand_imgs[1], Icon(op.join('src/data', op.basename(rand_imgs[1])), f"{op.basename(rand_imgs[1])}")),
        (rand_imgs[2], Icon(op.join('src/data', op.basename(rand_imgs[2])), f"{op.basename(rand_imgs[2])}")),
        (rand_imgs[3], Icon(op.join('src/data', op.basename(rand_imgs[3])), f"{op.basename(rand_imgs[3])}")),
        (rand_imgs[4], Icon(op.join('src/data', op.basename(rand_imgs[4])), f"{op.basename(rand_imgs[4])}"))
        ]
node_partial = build_dn_partial(list_img)
selected_image = None
render_selector = True


score = 0
prediction = "Please choose file from given test set"
true_label = ""

index_page = """
<|text-center|
<|{logo}|image|width=5vw|>


## Mosquito Detection and Classification App
##### Author: Ilona Kovaleva

|>
<|layout|columns=1 1 1 1|class_name=container align_columns_center|
<|
|>
<|
<|Change test set|button|on_action=manage_partial|>
##### Select image from test set:

<|part|partial={data_node_partial}|render={render_selector}|>


<|
|>
|>
<|
##### <|{f'{prediction}'}|>
##### <|{f'{true_label}'}|>
<|{img_path}|image|>

<|{score}|indicator|value={score}|min=0|max=100|width=25vw|>
|>
|>
"""

def on_change(state, var_name, var_value):
    if var_name== "selected_image":
        file_name = os.path.basename(var_value[0])
        notify(state, "info", "Sending message...")

        pred, prob = predict_image(file_name)
        notify(state, "info", "Predictions ready...")
        if pred != 'error':
            print(pred, prob)
            state.score = round(float(prob) * 100)
            state.prediction = "Prediction: " + pred
            state.img_path = state.selected_image[0]
            state.true_label = "Ground Truth: " + true_labels[file_name]
        else:
            state.score = 0
            state.prediction = "Error"
            state.img_path = state.selected_image[0]
            state.true_label = ""


def manage_partial(state):
    rand_imgs = []
    for _ in range(5):
        rand_imgs.append(test_images[random.randint(0, len(test_images)-1)])

    state.list_img = [(rand_imgs[0], Icon(op.join('src/data', op.basename(rand_imgs[0])), f"{op.basename(rand_imgs[0])}")),
        (rand_imgs[1], Icon(op.join('src/data', op.basename(rand_imgs[1])), f"{op.basename(rand_imgs[1])}")),
        (rand_imgs[2], Icon(op.join('src/data', op.basename(rand_imgs[2])), f"{op.basename(rand_imgs[2])}")),
        (rand_imgs[3], Icon(op.join('src/data', op.basename(rand_imgs[3])), f"{op.basename(rand_imgs[3])}")),
        (rand_imgs[4], Icon(op.join('src/data', op.basename(rand_imgs[4])), f"{op.basename(rand_imgs[4])}"))
        ]

    partial_content = build_dn_partial(state.list_img)
    state.data_node_partial.update_content(state, partial_content)


gui = tp.Gui(page=index_page)
data_node_partial = gui.add_partial(node_partial)

if __name__ == "__main__":
    app = tp.run(gui, title="MosAl Demo", run_server=True)
else:
    app = tp.run(gui, title="MosAl Demo", run_server=False)
