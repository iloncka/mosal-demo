# MosquitoScan Application v0.0.1

## Description

MosquitoScan is a demo mobile app designed to showcase the capability of classifying invasive mosquito species using machine learning. The app demonstrates the ability to accurately identify five specific invasive mosquito species through image classification. Users can capture images of mosquitoes and the app will provide real-time classification results, helping to raise awareness and facilitate the tracking of invasive mosquito populations.

## Installation Instructions
Follow these steps to install the mobile app on your Android device using the unsigned APK file from the release directory.

### Prerequisites
- Make sure that your Android device allows the installation of apps from unknown sources. To enable this, go to `Settings > Security` and check `Unknown sources`.
- Ensure that your device is running Android version 9.0 or higher.

### Steps
1. Download the APK file
   - Navigate to the release directory and download the correspondent APK file for the MosquitoScan App.
   - The directory path is `./release/` . This directory contains all the APK files for the MosquitoScan App, and the files will be named `app-arm64-v8a-release.apk` , `app-armeabi-v7a-release.apk` and `app-x86_64-release.apk`. You need choose the one that corresponds to your device. Use application `Droid Info` to find the device's architecture ('System' tab). Application `Droid Info` can be found in Google Play Store.

2. Transfer the APK to your Android device
   - Connect your device to your computer via USB cable and transfer the APK file to your device's storage.
   - Alternatively, you can download the APK file directly on your device if you've hosted it online.

3. Install the APK
   - Locate the APK file using a file manager on your device and tap on it to begin the installation.
   - If prompted, confirm the installation and proceed with the on-screen instructions.

4. Open the App
   - Once installed, open the app from your device's app drawer and start using it.


## Troubleshooting
If you encounter any installation issues, please make sure you've allowed the installation of apps from unknown sources as described in the Prerequisites. If problems persist, contact support at iloncka@gmail.com or Telegram @iloncka.

## License
Distributed under the Apache 2.0 License. See `LICENSE.txt` for more information.

## Contact Information
For any additional support or questions, please email me at iloncka@gmail.com or Telegram @iloncka