<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73  -->

<a name="readme-top"></a>

[![Forks][forks-shield]](https://gitlab.com/iloncka/mosal-demo/-/forks/new)
[![Stargazers][stars-shield]](https://gitlab.com/iloncka/mosal-demo/-/starrers)
[![GitLab Issues][issues-shield]](https://gitlab.com/iloncka/mosal-demo/-/issues)
[![MIT License][license-shield]](https://gitlab.com/iloncka/mosal-demo/-/blob/main/LICENSE.txt)

<!-- PROJECT LOGO -->

<br />
<div align="center">
  <a href="https://gitlab.com/iloncka/mosal-demo">
    <img src="images/logo-mosal.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Mosquito Detection and Classification App Demo</h3>

<p align="center">
    Detection and classification mosquitos tool
    <!-- <br />
    <a href="https://gitlab.com/iloncka/mosal-demo"><strong>Explore the docs »</strong></a>
    <br /> -->
    <br />
    <a href="https://gitlab.com/iloncka/mosal-demo">View Demo</a>
    ·
    <a href="https://gitlab.com/iloncka/mosal-demo/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/iloncka/mosal-demo/-/issues">Request Feature</a>
  </p>
</div>


[[_TOC_]]

<!-- ABOUT THE PROJECT -->

## About The Project

**Description of Mosquito Detection and Classification Models and Datasets Collection**

Our collection of models and datasets for mosquito detection and classification is a comprehensive resource designed for research and development in the field of entomology and epidemiology. Encompassing a diverse range of machine learning models and extensive datasets, this collection provides researchers and developers with the necessary tools to address the challenges of mosquito identification and classification.

The models in our collection consist of state-of-the-art machine learning algorithms, specifically trained on large volumes of data to ensure high accuracy in detecting and classifying various mosquito species. Including both classical machine learning models and modern deep neural networks, these models are capable of effectively operating even with images of low quality, making them valuable tools for use in various conditions.

The datasets presented in our collection comprise diverse images of mosquitoes, meticulously labeled and classified by professionals. These datasets cover various mosquito species as well as different stages of their life cycles, providing comprehensive coverage for model training and evaluation. With high-quality annotation and extensive data volume, our datasets serve as a reliable source for model training and testing their effectiveness.

In summary, our collection of models and datasets for mosquito detection and classification represents a valuable resource for researchers, developers, and experts in the fields of biology and epidemiology, providing them with the necessary tools to work with mosquitoes and prevent potential mosquito-borne diseases.

**Description of Mosquito Detection and Classification Web App**


The web application, built using the Taipy framework, is a powerful tool designed to demonstrate the classification of the five important mosquito species. It leverages the intuitive features of Taipy to create a user-friendly interface that allows for easy interaction and visualization of classification results.

Deployed on the Taipy Cloud free platform, the app offers cloud-based access for 2 hours, ensuring users can try the classification tool without any installation requirements. The application showcases the versatility and ease of use of the Taipy framework for developing and deploying machine learning applications.

Key features of the web app include:
- Real-time classification: Users can input data, and the app will instantly classify them into one of the five key species.
- Visual analytics: The app provides a graphical representation of the classification results, making it easy to understand and interpret the data.
- Taipy's interactive GUI: The web interface is built with Taipy's GUI components, offering a seamless and interactive experience.

Whether for educational purposes, research, or citizen science projects, this web app serves as an excellent example of how can machine learning can be applied in the field of entomology.

### Screenshots for Web App Demo

![Screenshot](images/taipy_cloud_deployment.png)

**Description of Mosquito Detection and Classification Mobile App**
MosquitoScan is a demo mobile app designed to showcase the capability of classifying invasive mosquito species using machine learning. The app demonstrates the ability to accurately identify five specific invasive mosquito species through image classification. Users can capture images of mosquitoes and the app will provide real-time classification results, helping to raise awareness and facilitate the tracking of invasive mosquito populations.
### Screenshots for Mobile App Demo

<img src="images/screen1.jpg" width=33%/> <img src="images/screen2.jpg" width=33%/> <img src="images/screen3.jpg" width=33%/> 



<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Built With

* Pytorch
* Flutter
* Docker
* Taipy

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Installation


1. Clone the repo
   ```sh
    git clone https://gitlab.com/iloncka/mosal-demo
   ```
2. Install python packages
   ```sh
    cd src
    pip3 install -r requirements.txt
   ```
3. Run application
   ```sh
    python src/main.py
   ```
4. Try application in your browser: http://127.0.0.1:5000 

<p align="right">(<a href="#readme-top">back to top</a>)</p> 

<!-- ROADMAP -->

## Roadmap

- [ ] Increase number of Mosquito Classes: Add more classes for epidemiologically dangerous mosquitoes. This will help users accurately identify mosquito species and assess risks.
- [ ] Include more Russian and CIS Mosquito Species:  Incorporate mosquito species commonly found in Russia and CIS countries. Enriching the app's database will make it more useful for users in this region.
- [ ] Expand Web App Features:
  - [ ] Add functionality for processing geodata
  - [ ] Add map visualization of points transmitted from mobile devices. Processing and displaying geodata will empower users to make informed decisions.
- [ ] Enhance Mobile App Functionality: Implement geodata transmission to a selected API. This feature will allow users to track mosquito locations and receive notifications about potential threats.
- [ ] Create open-source library with trained modelf for ease of usage for scientist in their work


See the [open issues](https://github.com/github_username/repo_name/issues) for a full list of proposed features (and known issues).


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the Apache 2.0 License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Ilona Kovaleva - iloncka@gmail.com

Project Link: [https://gitlab.com/iloncka/mosal-demo](https://gitlab.com/iloncka/mosal-demo)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[forks-shield]: https://img.shields.io/gitlab/forks/iloncka/mosal-demo.svg?style=for-the-badge
[stars-shield]: https://img.shields.io/gitlab/stars/iloncka/mosal-demo.svg?style=for-the-badge
[issues-shield]: https://img.shields.io/gitlab/issues/all/https%3A%2F%2Fgitlab.com%2Filoncka%2Fmosal-demo

[license-shield]: https://img.shields.io/gitlab/license/iloncka/mosal-demo.svg?style=for-the-badge
