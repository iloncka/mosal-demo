# MosquitoScan Web App Demo deployment
Folder `cloud_deployment` contains the source code for the deployment of the web app demo. The folder structure is as follows:
- `api`: contains the source code of the REST API, that handles the communication with the web app. This code is deploed to the Hugging Face Spaces.
- `ui`: contains the source code of the web app user interface. This code is deployed to the Taipy Cloud.
