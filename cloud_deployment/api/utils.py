from huggingface_hub import hf_hub_download
from fastai.vision.all import *
from fastai.learner import load_learner
import os
import os.path as op
import pickle

import platform
from contextlib import contextmanager
import pathlib

@contextmanager
def set_posix_windows():
    posix_backup = pathlib.PosixPath
    try:
        pathlib.PosixPath = pathlib.WindowsPath
        yield
    finally:
        pathlib.PosixPath = posix_backup

HOME_DIR = os.getcwd()
DATA_DIR =  op.join(HOME_DIR, 'data')
MODELS_DIR = op.join(HOME_DIR,'models')


def load_model():
    if platform.system() == "Windows":
        with set_posix_windows():
            learn = load_learner(op.join(MODELS_DIR, "vit_tiny_patch16_224_2.pkl"))
    else:
        learn = load_learner(op.join(MODELS_DIR, "vit_tiny_patch16_224_2.pkl"))
    return learn

# learn = load_learner(op.join(MODELS_DIR, "vit_tiny_patch16_224_2.pkl"))
def load_image(image_name):
    img = op.join(DATA_DIR, image_name)
    return img
