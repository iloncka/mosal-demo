---
title: MosAl-api
emoji: 🐨
colorFrom: purple
colorTo: pink
sdk: docker
pinned: false
license: apache-2.0
---
Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
