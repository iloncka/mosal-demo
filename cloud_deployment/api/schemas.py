from typing import List
from pydantic import BaseModel

class ClassificationResult(BaseModel):
    prediction: str
    score: float
